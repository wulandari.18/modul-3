import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import Tugas12 from "./Tugas/Tugas-12-ReactNative-Component/App";
import Tugas13_Register from "./Tugas/Tugas-13-React_Native-Styling/RegisterScreen";
import Tugas13_Login from "./Tugas/Tugas-13-React_Native-Styling/LoginScreen";
import Tugas13_About from "./Tugas/Tugas-13-React_Native-Styling/AboutScreen";
import Tugas14 from "./Tugas/Tugas14-React_Native-Component Api & Lifecycle/App";
import Tugas15 from "./Tugas/Tugas15-React_Native-Navigation/index";
import Tugas15_2 from "./Tugas/Tugas15-React_Native-Navigation/Tugas15-No2/index";
import Quiz3 from "./Quiz3/index";
export default function App() {
  return (
    //<Tugas12></Tugas12>
    //<Tugas13_Register></Tugas13_Register>
    //<Tugas13_Login></Tugas13_Login>
    //<Tugas13_About></Tugas13_About>
    //<Tugas14></Tugas14>
    //<Tugas15></Tugas15>
    //<Tugas15_2></Tugas15_2>
    <Quiz3></Quiz3>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
